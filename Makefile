bin/bytes: obj/taller5Bytes.o
	gcc $^ -o bin/bytes 

obj/taller5Bytes.o: src/taller5Bytes.c
	gcc -Wall -c $^ -o obj/taller5Bytes.o -g

.PHONY: clean
clean:
	rm bin/* obj/*

.PHONY: run
run: bin/bytes
	bin/bytes
