#include <stdio.h>

void verBytes(void *valor, long tamano);


int main(int argc, char **argv){
	int a = 1200;
	printf("Entero 1200 en bytes:\n");
	verBytes(&a,sizeof(a));
	a = 7899;
	printf("Entero 7899 en bytes:\n");
	verBytes(&a,sizeof(a));
	char p = 'b';
	printf("caracter b en bytes:\n");
	verBytes(&p,sizeof(p));
	short q = 788;
	printf("Short 788 en bytes:\n");
	verBytes(&q,sizeof(q));
	long q2 = 8900000000;
	printf("Long 8900000000 en bytes:\n");
	verBytes(&q2,sizeof(q2));
	float w = 4.5f;
	printf("float 4.5 en bytes:\n");
	verBytes(&w,sizeof(w));
	double m = 17.6;
	printf("double 17.6 en bytes:\n");
	verBytes(&m,sizeof(m));


	typedef struct datos{
	int a;
	short p;
	char *w;
	char *msg;
	}teststruct;

	teststruct n;
	n.a = 0xffffffff;
	n.p = 0xbbae;
	n.w = 'z';
	n.msg = "hola";

	
	printf("Entero de la estructura en bytes:\n");
	verBytes(&(n.a),sizeof(n.a));
	printf("caracter w en bytes:\n");
	verBytes(&(n.w),sizeof(n.w));
	printf("caracter msg en bytes:\n");
	verBytes(&(n.msg),sizeof(n.msg));
	printf("Short p de la estructura en bytes:\n");
	verBytes(&(n.p),sizeof(n.p));




	return 0;
}

void verBytes(void *valor, long tamano){
	unsigned char *byte=(unsigned char*)valor;
	for(int i=0; i<tamano;i++){	
		printf("%p 0x%.2x\n", (void*)byte+i, *(byte+i));
	}
	printf("\n");
}